#include <stdlib.h>
#include <math.h>
#include "Shape.h"
#include "Random.h"

class Gene
{
public:

	Gene();
	Gene(Gene*);
	Gene(Gene*, Gene*);
	~Gene();
	int getNumShapes();
	Shape* getShape(int);
	Gene* breed(Gene*);
	void mutate(int);
	
private:

	Shape **genetic_data;
	int num_shapes;

};
Gene::Gene()
{
	//-----------------------NUMBER OF SHAPES---------------------------
	num_shapes = ceil(random_range(0,5));

	genetic_data = new Shape * [num_shapes];
	for (int i = 0; i < num_shapes; i++)
	{	
		//--------------------NUMBER OF POINTS---------------------------
		int num_points = ceil(random_range(3,8)) * 2;
		genetic_data[i] = new Shape(num_points);
	}
}
Gene::Gene(Gene* clone)
{
	//-----------------------NUMBER OF SHAPES---------------------------
	num_shapes = clone->getNumShapes();

	genetic_data = new Shape * [num_shapes];
	for (int i = 0; i < num_shapes; i++)
	{	
		genetic_data[i] = clone->getShape(i);
	}
}
Gene::Gene(Gene* parent1, Gene* parent2)
{
	float probability_of_crossover = random_range(0,1);

		if (probability_of_crossover < .3)
		{
		//-----------------------NUMBER OF SHAPES---------------------------
		int p1NumShapes = parent1->getNumShapes();
		int p2NumShapes = parent2->getNumShapes();
		
		num_shapes = floor(random_range(1,(p1NumShapes+p2NumShapes)));

		genetic_data = new Shape * [num_shapes];
		for (int i = 0; i < num_shapes; i++)
		{	
			if (i < p1NumShapes)
			{
				genetic_data[i] = new Shape(parent1->getShape(i));
			}
			else
			{
				genetic_data[i] = new Shape(parent2->getShape(i-p1NumShapes));
			}
		}
	}
	else if (probability_of_crossover < .6)
	{
		//-----------------------NUMBER OF SHAPES---------------------------
		int p1NumShapes = parent1->getNumShapes();
		int p2NumShapes = parent2->getNumShapes();
		
		num_shapes = floor(random_range(p2NumShapes,(p1NumShapes+p2NumShapes)));

		genetic_data = new Shape * [num_shapes];
		for (int i = 0; i < num_shapes; i++)
		{	
			if (i < p1NumShapes)
				genetic_data[i] = new Shape(parent1->getShape(i));
			else
				genetic_data[i] = new Shape(parent2->getShape(i-p1NumShapes));
		}
	}
	else if (probability_of_crossover < .8)
	{
		//-----------------------NUMBER OF SHAPES---------------------------
		int p1NumShapes = parent1->getNumShapes();
		
		num_shapes = p1NumShapes;

		genetic_data = new Shape * [num_shapes];
		for (int i = 0; i < num_shapes; i++)
		{
			genetic_data[i] = new Shape(parent1->getShape(i));
		}
	}
	else
	{
		//-----------------------NUMBER OF SHAPES---------------------------
		int p2NumShapes = parent2->getNumShapes();
		
		num_shapes = p2NumShapes;

		genetic_data = new Shape * [num_shapes];
		for (int i = 0; i < num_shapes; i++)
		{
			genetic_data[i] = new Shape(parent2->getShape(i));
		}
	}
}
Gene::~Gene()
{
	delete[] genetic_data;
}
int Gene::getNumShapes()
{
	return num_shapes;
}
Shape* Gene::getShape(int i)
{
	return genetic_data[i];
}
Gene* Gene::breed(Gene* mate)
{
	return new Gene(this, mate);
	//child->mutate();

	//Shape* m1 = this->getShape();
	//Shape* m2 = mate->getShape();
}
void Gene::mutate(int degree)
{
	for (int i = 0; i < num_shapes; i++)
	{
		genetic_data[i]->mutate(degree);
	}
}