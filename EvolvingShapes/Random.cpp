#include "Random.h"

float random_range(float a, float b)
{
	if (a == b)
		return a;
	return ((b-a)*((float)rand()/RAND_MAX))+a;
}

float mutation(float curr, int degree)
{
	float rand_num = random_range(-1,1);
	if (rand_num > -.15 && rand_num < 0)
		return curr - degree;
	else if (rand_num < .15 && rand_num > 0)
		return curr + degree;
	else if (rand_num < 0)
		return curr + degree/3;
	else
		return curr - degree/3;
}