#include <GL/glut.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <time.h>
#include "Gene.h"
#include "Random.h"

using namespace std;

static void redraw(void);
int main(int argc, char **argv);
void openglInitSection();
void drawGene(Gene* gene);
void processMouse(int button, int state, int x, int y);
int parseClick(int, int); // returns 1 through num_genes based on x,y coordinate of mouse click
// create genes array
Gene **genes;
int num_genes = 9;
int selectedGene = -1;
int degree_of_mutation = 5;
int num_mutations = 0;

int main(int argc, char **argv)
{
	
	// initialize open GL stuff
	glutInit(&argc,argv);
	openglInitSection();
	
	srand(time(NULL));

	// create 9 genes
	
	genes = new Gene * [num_genes];
	for (int i = 0; i < num_genes; i++)
	{
		genes[i] = new Gene();
	}
	
	//-----------------------------------------------------------------
	glutMainLoop();               //the main loop of the GLUT framework

	delete[] genes;
	return 0;
}
void openglInitSection()
{
	glutInitWindowSize(900,900);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutCreateWindow("Evolving Shapes");
	
	glutDisplayFunc(redraw); 

	glutMouseFunc(processMouse);

	
	// set up screen
	glMatrixMode(GL_PROJECTION);

	//sets up the projection matrix for a perspective transform
	/*gluPerspective(45,
					 1.0,
					 10.0,
					 500.0);
*/
	glOrtho(0,300,300,0,0,1);
	glMatrixMode(GL_MODELVIEW); 
}
static void redraw(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glPushMatrix();
	glTranslatef(50,250,0);
	
	// draw the genes
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			drawGene(genes[i*3+j]);
			glTranslatef(0,-100,0);
		}
		glTranslatef(100,300,0);
	}

	glPopMatrix();
	glutSwapBuffers();
}

void drawGene(Gene* gene)
{
	for (int i = 0; i < gene->getNumShapes(); i++)
	{
		Shape* s = gene->getShape(i);
		glColor3f(s->getColor()->red, (s->getColor())->green, s->getColor()->blue);
		glBegin(GL_POLYGON);
		for (int i = 0; i < s->getNumPoints(); i+=2)
		{
			glVertex3f(s->getPoint(i),s->getPoint(i+1),0);
		}
		glEnd();
	}
}

void processMouse(int button, int state, int x, int y) 
{
	if (state == GLUT_DOWN && button == GLUT_LEFT_BUTTON)
	{
		int selected = parseClick(x, y);
		
		if (selectedGene >= 0)
		{
			Gene* m1 = new Gene(genes[selected]);
			Gene* m2 = new Gene(genes[selectedGene]);

			for (int i = 0; i < num_genes; i++)
			{
				if (i != selected && i != selectedGene)
				{
					genes[i] = m1->breed(m2);
					genes[i]->mutate(degree_of_mutation);
				}
			}
			
			selectedGene = -1;
			num_mutations++;
			degree_of_mutation = 14 - floor(log((float)num_mutations)) * 3;
			if (degree_of_mutation < 1)
				degree_of_mutation = 1;
		}
		else
		{
			selectedGene = selected;
		}
	}
	if (state == GLUT_DOWN && button == GLUT_RIGHT_BUTTON)
	{
		for (int i = 0; i < num_genes; i++)
		{
			genes[parseClick(x,y)] = new Gene();
		}
	}
	glutPostRedisplay();
}

/** preconditions: x and y are integers corresponding to a position on the 900x900 screen
*      			   we are assuming that the genes are laid out in a 3x3 grid
*   postcondition: have returned the array index corresponding to screen click as follows
*					|-----|
*                   |2|5|8|
*                   |1|4|7|
*                   |0|3|6|
*                   |-----|
*   error-handling: return 8 if the index is out of range [0-num_genes]
**/
int parseClick(int x, int y)
{
	int i = x / 300;
	int j = (900-y) / 300;
	
	return (i*3+j >= num_genes || i*3+j < 0) ? 8 : (i*3+j);
}