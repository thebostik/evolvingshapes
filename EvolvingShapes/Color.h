#include <math.h>
#include "Random.h"

class Color
{
private:
public:
	Color();
	Color(Color*);
	float red;
	float green;
	float blue;
	void mutate(int);
};

Color::Color()
{
	red = random_range(0,1);
	green = random_range(0,1);
	blue = random_range(0,1);
}
Color::Color(Color* toCopy)
{
	red = toCopy->red;
	green = toCopy->green;
	blue = toCopy->blue;
}
void Color::mutate(int degree)
{
	red += random_range(-.03*degree,.03*degree);
	green += random_range(-.03*degree,.03*degree);
	blue += random_range(-.03*degree,.03*degree);
}