#include <stdlib.h>
#include "Color.h"
#include "Random.h"

class Shape
{
public:
	Shape();
	Shape(int);
	Shape(Shape*);
	~Shape();
	int getNumPoints();
	float getPoint(int);
	Color *getColor();
	void mutate(int);
private:
	int numPoints;
	float* points;
	Color* color;
};

Shape::Shape()
{
	Shape::Shape(6);
}
Shape::Shape(int num)
{
	color = new Color();
	numPoints = num;
	points = new float[num];

	for (int i = 0; i < num; i++)
	{
		points[i] = random_range(-30,30);
	}
}	
Shape::Shape(Shape* s)
{
	color = s->getColor();
	numPoints = s->getNumPoints();
	points = new float[numPoints];
	for (int i = 0; i < numPoints; i++)
	{
		points[i] = s->getPoint(i);
	}
	//delete s;
}

Shape::~Shape()
{
	delete[] points;
	delete color;
}
int Shape::getNumPoints()
{
	return numPoints;
}
float Shape::getPoint(int point)
{
	return points[point];
}
Color* Shape::getColor()
{
	return new Color(color);
}
void Shape::mutate(int degree)
{  
	color->mutate(degree);
	for (int i = 0; i < numPoints; i++)
	{
		points[i] = mutation(points[i],degree);
	}	
}